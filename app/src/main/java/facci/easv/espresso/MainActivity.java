package facci.easv.espresso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.PatternsCompat;

import android.graphics.PathEffect;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

        private Button button;
        private EditText editTextEmail, editTextPass;
        private User user;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
                setViews();
                user = new User("a@gmail.com", "1234");
        }

        private void setViews() {
                button = findViewById(R.id.button);
                button.setOnClickListener(this);
                editTextEmail = findViewById(R.id.editTextUser);
                editTextPass = findViewById(R.id.editTextPass);
        }

        private Boolean User() {
                return Objects.equals(user.getEmail(), editTextEmail.getText().toString());
        }

        private Boolean Email() {
                return !editTextEmail.getText().toString().isEmpty();
        }
        private Boolean Password() {
                return !editTextPass.getText().toString().isEmpty();
        }

        private Boolean ValidateEmail()  {
                Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                Matcher match = pattern.matcher(editTextEmail.getText().toString());
                return match.find();
        }

        @Override
        public void onClick(View view) {
                if (view == button){
                        if (!Email() || !Password()){
                                Toast.makeText(this, "Los campos no deben estar vacíos", Toast.LENGTH_SHORT).show();
                        }else {
                                if (!ValidateEmail()){
                                        Toast.makeText(this, "El correo debe ser válido", Toast.LENGTH_SHORT).show();
                                }else{
                                        if (!User()){
                                                Toast.makeText(this, "No se encontró el usuario", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                                Toast.makeText(this, "Sesión iniciada", Toast.LENGTH_SHORT).show();
                                        }
                                }
                        }
                }
        }
}
